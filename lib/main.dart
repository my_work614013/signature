import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_signaturepad/signaturepad.dart';
import 'dart:ui' as ui;
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:open_file/open_file.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'dart:developer';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'sign your name'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<SfSignaturePadState> _signaturePadKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      body: Center(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Text.rich(
                TextSpan(
                  text: "Signature",
                  style: TextStyle(
                    color: Colors.pinkAccent,
                    fontSize: 50,
                  ),
                  children: [
                    TextSpan(
                      text: "*",
                      style: TextStyle(
                        color: Colors.redAccent,
                        fontSize: 45,
                      ),
                    ),
                  ],
                ),
              ),
              const Text(
                "Please sign your signature",
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 20,
                ),
              ),
              const Padding(padding: EdgeInsets.only(top: 10)),
              SizedBox(
                width: double.infinity,
                height: 350,
                child: SfSignaturePad(
                  minimumStrokeWidth: 3.0,
                  maximumStrokeWidth: 6.0,
                  backgroundColor: Colors.white,
                  strokeColor: Colors.black,
                  key: _signaturePadKey,
                ),
              ),
              const Padding(padding: EdgeInsets.only(top: 10)),
              ListTile(
                title: Row(
                  children:  <Widget>[
                    SizedBox(
                      width: 100,
                      height: 45,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white,
                          onPrimary: Colors.blue,
                          side: const BorderSide(
                            width: 1,
                            color: Colors.blue,
                          ),
                        ),
                        onPressed: () async {
                          _signaturePadKey.currentState!.clear();
                        },
                        child: const Text("Clear"),
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(left: 20)),
                    SizedBox(
                      width: 225,
                      height: 45,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.blue,
                          onPrimary: Colors.white,
                          side: const BorderSide(
                            width: 1,
                            color: Colors.blue,
                          ),
                        ),
                        onPressed: () async {
                          ui.Image image = await _signaturePadKey.currentState!.toImage();
                          final byteData = await image.toByteData(format: ui.ImageByteFormat.png);
                          final Uint8List imageByte = byteData!.buffer.asUint8List(byteData.offsetInBytes, byteData.lengthInBytes);
                          final String path = (await getApplicationSupportDirectory()).path;
                          final String fileName = '$path/Output.png';
                          final File file = File(fileName);
                          await file.writeAsBytes(imageByte, flush: true);
                          OpenFile.open(fileName);
                          final base64String = base64.encode(imageByte);
                          log("data, ------------");
                          log("data, $base64String");
                        },
                        child: const Text("Save as Image"),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}